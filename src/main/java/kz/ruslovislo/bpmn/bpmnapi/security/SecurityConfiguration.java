package kz.ruslovislo.bpmn.bpmnapi.security;

/**
 * @author Ruslan Temirbulatov on 11/4/19
 * @project telem-api
 */


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

//@Configuration
//@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

//    @Autowired
//    private UserMnGmNtApiService userMnGmNtApiService;

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        httpSecurity.csrf().disable().cors().and()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/ma/**").permitAll()
                .and().authorizeRequests()
//                .antMatchers(HttpMethod.GET,"/","/**","/**/*","/**/**/*","/**/**/**/*")
                .antMatchers(HttpMethod.GET,"/ma/who-am-i","/**")
                .permitAll()
                .and()
                .addFilterAfter(new SecurityFilter(), BasicAuthenticationFilter.class)
//                .addFilterAfter(new TelemApiFilter(userMnGmNtApiService), BasicAuthenticationFilter.class)
                    ;
    }

}
