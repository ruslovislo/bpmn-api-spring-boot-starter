package kz.ruslovislo.bpmn.bpmnapi.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

//import org.activiti.engine.ProcessEngines;
//import org.activiti.engine.history.NativeHistoricProcessInstanceQuery;
//import org.activiti.engine.impl.AbstractQuery;
//import org.activiti.engine.query.NativeQuery;
//import org.activiti.engine.query.Query;
//import org.activiti.engine.query.QueryProperty;
import kz.ruslovislo.pojo.table.TableDataRequest;
import org.flowable.common.engine.api.query.Query;
import org.flowable.engine.history.NativeHistoricProcessInstanceQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;


public class ActivitiUtils {

	
	public static <T,X>Map<String,Object> pageToRestDatatable(TableDataRequest request, Query query, TableDataRequest.IBeanConverter<T,X> converter)throws Exception{
		
		long count = query.count();
		 
		if(request.getFirst()==null && request.getStart()!=null && request.getLength()!=null) {
			request.setFirst(request.getStart() * request.getLength());
		}


		
		
		List list = query.listPage(request.getFirst(), request.getLength());
		List<T> records = new LinkedList<>();
		
		for(Object o:list) {
			records.add(converter.convert((X)o));
		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("recordsTotal", count);
		map.put("recordsFiltered", count);
		map.put("data", records);
		return map;
	}
	
	public static <T, Q extends Query>Map<String,Object> pageToRestDatatable(TableDataRequest request, NativeHistoricProcessInstanceQuery countQuery, NativeHistoricProcessInstanceQuery query, TableDataRequest.IBeanConverter<T, Q> converter)throws Exception{
		
		
//		NativeHistoricProcessInstanceQuery query =ProcessEngines.getDefaultProcessEngine().getHistoryService().createNativeHistoricProcessInstanceQuery().sql("select count(z.*) from ("+sql+") z");
		
		
		
		
		long count = countQuery.count();
		
//		query =ProcessEngines.getDefaultProcessEngine().getHistoryService().createNativeHistoricProcessInstanceQuery().sql(sql);
		
		
		if(request.getFirst()==null && request.getStart()!=null && request.getLength()!=null) {
			request.setFirst(request.getStart() * request.getLength());
		}
			
		
		
		List list = query.listPage(request.getFirst(), request.getLength());
//		List<T> records = new LinkedList<>();


		List<T> records = new LinkedList<>();
		for(Object o:list){
			records.add(converter.convert((Q)o));
		}


//		Query q = converter.toQuery(list);
//		list = q.listPage(request.getFirst(), request.getLength());
//		
//		for(Object o:list) {
//			records.add(converter.convert(o));
//		}
		
		Map<String, Object> map = new HashMap<>();
		map.put("recordsTotal", count);
		map.put("recordsFiltered", count);
		map.put("data", records);
		return map;
	}
	
	
	public static PageRequest getPageRequest(TableDataRequest request) {
		
		if(request.getStart()==null) {
			request.setStart(request.getFirst()/request.getLength());
		}
		
		return getPageRequest(request.getStart(),request.getLength(),request.getSortDirection(),request.getSortField());
	}
	public static PageRequest getPageRequest(Integer start,Integer length, Direction direction, String sortName) {
		if(sortName==null || sortName.trim().length()==0)
			return new PageRequest(start, length);
		else
			return new PageRequest(start, length, new Sort(direction, sortName));
	}
	
	
}
