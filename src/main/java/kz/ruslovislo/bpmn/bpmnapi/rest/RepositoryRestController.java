package kz.ruslovislo.bpmn.bpmnapi.rest;

import kz.ruslovislo.bpmn.bpmnapi.model.pojo.AProcessDefinition;
import kz.ruslovislo.bpmn.bpmnapi.utils.ActivitiUtils;
import kz.ruslovislo.pojo.table.TableDataRequest;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path="/repository")
public class RepositoryRestController {


	@GetMapping("/process-definitions/list")
	public List<AProcessDefinition> processDefinitionsLatest(){
		ProcessDefinitionQuery query = ProcessEngines.getDefaultProcessEngine().getRepositoryService().createProcessDefinitionQuery().latestVersion();
		List<AProcessDefinition> list = new LinkedList<>();
		for(ProcessDefinition pd:query.list()) {
			list.add(AProcessDefinition.parse(pd));
		}
		return list;
		
	}

	@PostMapping("/process-definitions/data")
	public Map<String,Object> processDefinitionsLatest(@RequestBody TableDataRequest request)throws Exception{
		ProcessDefinitionQuery query = ProcessEngines.getDefaultProcessEngine().getRepositoryService().createProcessDefinitionQuery().latestVersion();

		return ActivitiUtils.pageToRestDatatable(request, query, new TableDataRequest.IBeanConverter<AProcessDefinition, ProcessDefinition>() {
			@Override
			public AProcessDefinition convert(ProcessDefinition processDefinition) {
				return AProcessDefinition.parse(processDefinition);
			}
		});

	}

	@PostMapping("/deploy-xml")
	public String deploy(@RequestParam("file") MultipartFile file) throws Exception {

		RepositoryService repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();

		Deployment deployment = repositoryService.createDeployment()
				.name(file.getOriginalFilename())
				.addInputStream(file.getOriginalFilename(), file.getInputStream())
				.deploy();

		return deployment.getId();
	}

	@PostMapping("/undeploy")
	public String undeploy(String id)throws Exception{
		RepositoryService repositoryService = ProcessEngines.getDefaultProcessEngine().getRepositoryService();
		repositoryService.deleteDeployment(id);
		return "true";
	}

	@GetMapping("/test")
	public String test(){
		return "test";
	}


	
}
