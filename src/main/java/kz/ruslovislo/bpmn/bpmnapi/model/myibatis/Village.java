package kz.ruslovislo.bpmn.bpmnapi.model.myibatis;

public class Village {

	private String proc_inst_id_;
	private String task_id_;
	private String var_id_;
	private String var_name_;
	private String var_type_;
	private String value_;
	private String information_;
	
	public String getProc_inst_id_() {
		return proc_inst_id_;
	}
	public void setProc_inst_id_(String proc_inst_id_) {
		this.proc_inst_id_ = proc_inst_id_;
	}
	public String getTask_id_() {
		return task_id_;
	}
	public void setTask_id_(String task_id_) {
		this.task_id_ = task_id_;
	}
	public String getVar_id_() {
		return var_id_;
	}
	public void setVar_id_(String var_id_) {
		this.var_id_ = var_id_;
	}
	public String getVar_name_() {
		return var_name_;
	}
	public void setVar_name_(String var_name_) {
		this.var_name_ = var_name_;
	}
	public String getVar_type_() {
		return var_type_;
	}
	public void setVar_type_(String var_type_) {
		this.var_type_ = var_type_;
	}
	public String getValue_() {
		return value_;
	}
	public void setValue_(String value_) {
		this.value_ = value_;
	}
	public String getInformation_() {
		return information_;
	}
	public void setInformation_(String information_) {
		this.information_ = information_;
	}
	
	
	
}
