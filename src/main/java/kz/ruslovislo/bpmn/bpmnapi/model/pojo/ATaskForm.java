package kz.ruslovislo.bpmn.bpmnapi.model.pojo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import kz.ruslovislo.bpmn.bpmnapi.model.myibatis.CustomMybatisMapper;
import kz.ruslovislo.bpmn.bpmnapi.model.myibatis.Village;
import org.flowable.common.engine.impl.cmd.CustomSqlExecution;
import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.form.FormProperty;
import org.flowable.engine.form.TaskFormData;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.impl.cmd.AbstractCustomSqlExecution;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import lombok.Data;

@SuppressWarnings("serial")
@Data
public class ATaskForm implements Serializable {

	private final static Logger log =LoggerFactory.getLogger(ATaskForm.class);
	
	private ATask task;

	private List<AFormProperty> properties = new LinkedList<>();

	public static ATaskForm parse(Task task) {

		TaskFormData formData = ProcessEngines.getDefaultProcessEngine().getFormService().getTaskFormData(task.getId());

		ATaskForm atf = new ATaskForm();
		atf.setTask(ATask.parse(task));
		List<FormProperty> props = formData.getFormProperties();
		if (props != null) {
			for (FormProperty prop : props) {
				atf.getProperties().add(AFormProperty.parse(prop));
			}
		}
		return atf;
	}

	public static ATaskForm parse(HistoricTaskInstance task) {
		ATaskForm atf = new ATaskForm();
		try {
			atf.setTask(ATask.parse(task));
			
			
			CustomSqlExecution<CustomMybatisMapper, List<Village>> customSqlExecution =
				    new AbstractCustomSqlExecution<CustomMybatisMapper, List<Village>>(CustomMybatisMapper.class) {
				 
				  public List<Village> execute(CustomMybatisMapper customMapper) {
					  Village village = new Village();
					  village.setProc_inst_id_(task.getProcessInstanceId());
					  village.setTask_id_(task.getId());
					  return customMapper.selectTaskVariables(village);
				  }
				 
				};
				
				List<Village> results = ProcessEngines.getDefaultProcessEngine().getManagementService().executeCustomSql(customSqlExecution);
				
				
			for(Village result:results) {
				atf.getProperties().add(AFormProperty.parse(result));
			}


		} catch (Exception ex) {
			ex.printStackTrace();
			log.error("Error parsing ATaskForm from HistoricTaskInstance with id "+task.getId(),ex);
			
		}
		return atf;
	}
}
