package kz.ruslovislo.bpmn.bpmnapi.model.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;



import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.history.HistoricDetail;
import org.flowable.task.api.history.HistoricTaskInstance;
import org.flowable.variable.api.history.HistoricVariableInstance;

@SuppressWarnings("serial")
@Data
public class AHistoricTaskInstance implements Serializable{

	private String name;
	private String assignee;
	@JsonFormat(pattern="d MMM 'в' HH:mm", locale="RU")
	private Date createTime;
	@JsonFormat(pattern="d MMM 'в' HH:mm", locale="RU")
	private Date startTime;
	@JsonFormat(pattern="d MMM 'в' HH:mm", locale="RU")
	private Date endTime;
	@JsonFormat(pattern="d MMM 'в' HH:mm", locale="RU")
	private Date claimTime;
	private String owner;
	private String processDefinitionId;
	private String taskDefinitionKey;
	private String processInstanceId;
	private Map<String, Object> localVariables = new HashMap<>();
	private Map<String, Object> processVariables = new HashMap<>();
	private List<HistoricDetail> historicDetails = new LinkedList<>();
	private ATaskForm form;
	private String id;
	
	
	
	public static AHistoricTaskInstance parse(HistoricTaskInstance task) {
		AHistoricTaskInstance hist = new AHistoricTaskInstance();
		hist.setName(task.getName());
		hist.setAssignee(task.getAssignee());
		hist.setCreateTime(task.getCreateTime());
		hist.setEndTime(task.getEndTime());
		hist.setOwner(task.getOwner());
		hist.setStartTime(task.getStartTime());
		hist.setClaimTime(task.getClaimTime());
		hist.setLocalVariables(task.getTaskLocalVariables());
		hist.setProcessInstanceId(task.getProcessInstanceId());
		hist.setProcessVariables(task.getProcessVariables());
		
		hist.setProcessVariables(new HashMap<>());
		List<HistoricVariableInstance> vars = ProcessEngines.getDefaultProcessEngine().getHistoryService().createHistoricVariableInstanceQuery().processInstanceId(task.getProcessInstanceId()).list();
		if(vars!=null)
			for(HistoricVariableInstance v:vars) {
				hist.getProcessVariables().put(v.getVariableName(), v.getValue());
			}
		 

		
		
		
		
		hist.setProcessDefinitionId(task.getProcessDefinitionId());
		hist.setTaskDefinitionKey(task.getTaskDefinitionKey());
		hist.setId(task.getId());
		
		List<HistoricActivityInstance> actInstances = ProcessEngines.getDefaultProcessEngine().getHistoryService().createHistoricActivityInstanceQuery().processInstanceId(task.getProcessInstanceId()).list();
		if(actInstances!=null) {
			for(HistoricActivityInstance act:actInstances) {
				if(act.getTaskId()!=null && act.getTaskId().equals(task.getId())) {
					
					List<HistoricDetail> dlist = ProcessEngines.getDefaultProcessEngine().getHistoryService().createHistoricDetailQuery().variableUpdates().activityInstanceId(act.getId()).list();
					for(HistoricDetail d:dlist) {
						hist.getHistoricDetails().add( d);
					}
					
					break;
				}
			}
		}
		
		
		return hist;
	}
	
	public static void main(String[] args) {
		System.out.println(new Date(1567004795167L));
	}
}
