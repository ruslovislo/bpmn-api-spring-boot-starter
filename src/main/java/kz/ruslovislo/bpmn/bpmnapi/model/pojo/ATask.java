package kz.ruslovislo.bpmn.bpmnapi.model.pojo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.flowable.common.engine.impl.identity.Authentication;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.flowable.task.api.history.HistoricTaskInstance;




import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@SuppressWarnings("serial")
@Data
public class ATask implements Serializable{

	private String status;
	
	private String id;
	private String name;
	private String assignee;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm")
	private Date createTime;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm")
	private Date claimTime;
	@JsonFormat(pattern="yyyy-MM-dd HH:mm")
	private Date endTime;
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date dueDate;
	private String category;
	private String description;
	private String executionId;
	private String formKey;
	private String owner;
	private String parentTaskId;
	private int priority;
	private String processDefinitionId;
	private String processInstanceId;
	private Map<String, Object> processVariables;
	private String taskDefinitionKey;
	private Map<String, Object> taskLocalVariables;
	private String tenantId;
	private AProcessInstance processInstance;
	
	private Map<String, String> formProperties = new HashMap<>();
	
	
	/**
	 * Найти актуальный TASK для моего аккаунта по выбранному процессу
	 * @param instance
	 * @return
	 */
	public static ATask findMyActualTask(ProcessInstance instance) {


		List<Task> list = ProcessEngines.getDefaultProcessEngine().getTaskService().createTaskQuery().processInstanceId(instance.getProcessInstanceId()).taskAssignee(Authentication.getAuthenticatedUserId()).list();
//		if(list==null || list.size()==0)
//			list = ProcessEngines.getDefaultProcessEngine().getTaskService().createTaskQuery().processInstanceId(instance.getProcessInstanceId()).taskCandidateGroupIn(BpmnSession.get().getUserGroups()).list();
			
		if(list!=null && list.size()>0)
			return ATask.parse(list.get(0));
		
		return null;
	}
	
	public static ATask parse(HistoricTaskInstance task) {
		ATask at = new ATask();
		if(task.getEndTime()!=null)
			at.setStatus("finished");
		else
			at.setStatus("unfinished");
		at.setId(task.getId());
		at.setAssignee(task.getAssignee());
		at.setCreateTime(task.getCreateTime());
		at.setClaimTime(task.getClaimTime());
		at.setEndTime(task.getEndTime());
		at.setName(task.getName());
		at.setProcessInstanceId(task.getProcessInstanceId());
		at.setDescription(task.getDescription());
		
		
		HistoricProcessInstance q =ProcessEngines.getDefaultProcessEngine().getHistoryService().createHistoricProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();

		at.setProcessInstance(AProcessInstance.parse(q));
		return at;
	}
	
	public static ATask parse(Task task) {
		ATask at = new ATask();
		at.setStatus("unfinished");
		at.setId(task.getId());
		at.setName(task.getName());
		at.setCreateTime(task.getCreateTime());
		at.setAssignee(task.getAssignee());
		at.setCategory(task.getCategory());
		at.setClaimTime(task.getClaimTime());
		at.setDescription(task.getDescription());
		at.setDueDate(task.getDueDate());
		at.setExecutionId(task.getExecutionId());
		at.setFormKey(task.getFormKey());
		at.setOwner(task.getOwner());
		at.setParentTaskId(task.getParentTaskId());
		at.setPriority(task.getPriority());
		at.setProcessDefinitionId(task.getProcessDefinitionId());
		at.setProcessInstanceId(task.getProcessInstanceId());
		at.setProcessVariables(task.getProcessVariables());
		at.setTaskDefinitionKey(task.getTaskDefinitionKey());
		at.setTaskLocalVariables(task.getTaskLocalVariables());
		at.setTenantId(task.getTenantId());
		
		
		
		
//		TaskFormData formData = ProcessEngines.getDefaultProcessEngine().getFormService().getTaskFormData(task.getId());
//		
//		List<FormProperty> props = formData.getFormProperties();
//		for(FormProperty prop:props) {
//			at.getFormProperties().put(prop.getId(), prop.getValue());
//		}
		
		
		ProcessInstance pi = ProcessEngines.getDefaultProcessEngine().getRuntimeService().createProcessInstanceQuery().processInstanceId(task.getProcessInstanceId()).singleResult();
		at.setProcessInstance(AProcessInstance.parse(pi));
		
		
		
		
		return at;
	}
}
