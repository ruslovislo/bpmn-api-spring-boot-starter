package kz.ruslovislo.bpmn.bpmnapi.model.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;




import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.ProcessInstance;

@SuppressWarnings("serial")
@Data
public class AProcessInstance implements Serializable{

	private String id;
	private String activityId;
	private String businessKey;
	private String deploymentId;
	private String description;
	private String localizedDescription;
	private String name;
	private String localizedName;
	private String parentId;
	private String processDefinitionId;
	private String processDefinitionKey;
	private String processDefinitionName;
	private Integer processDefinitionVersion;
	private String processInstanceId;
	private String rootProcessInstanceId;
	private Map<String, Object> processVariables;
	
	@JsonFormat(pattern="d MMM 'в' HH:mm", locale="RU")
	private Date startTime;
	private String startUserId;
	private String superExecutionId;
	private String tenantId;
	
	public static AProcessInstance parse(ProcessInstance instance) {
		AProcessInstance api = new AProcessInstance();
		api.setActivityId(instance.getActivityId());
		api.setBusinessKey(instance.getBusinessKey());
		api.setDeploymentId(instance.getDeploymentId());
		api.setDescription(instance.getDescription());
		api.setId(instance.getId());
		api.setName(instance.getProcessDefinitionName());
		
		api.setLocalizedName(instance.getLocalizedName());
		api.setLocalizedDescription(instance.getLocalizedDescription());
		api.setParentId(instance.getParentId());
		api.setProcessDefinitionId(instance.getProcessDefinitionId());
		api.setProcessDefinitionKey(instance.getProcessDefinitionKey());
		api.setProcessDefinitionName(instance.getProcessDefinitionName());
		api.setProcessDefinitionVersion(instance.getProcessDefinitionVersion());
		api.setProcessInstanceId(instance.getProcessInstanceId());
		api.setRootProcessInstanceId(instance.getRootProcessInstanceId());
		api.setStartTime(instance.getStartTime());
		api.setStartUserId(instance.getStartUserId());
		api.setSuperExecutionId(instance.getSuperExecutionId());
		api.setTenantId(instance.getTenantId());
		
		
		if(!instance.isEnded())
			api.setProcessVariables(ProcessEngines.getDefaultProcessEngine().getRuntimeService().getVariables(instance.getProcessInstanceId()));
		
		return api;
	}
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("d MMM 'в' HH:mm");
	
	public static AProcessInstance parse(HistoricProcessInstance instance) {
		AProcessInstance api = new AProcessInstance();
		
		api.setActivityId(null);
		api.setBusinessKey(instance.getBusinessKey());
		api.setDeploymentId(instance.getDeploymentId());
		api.setDescription(instance.getDescription());
		api.setId(instance.getId());
		api.setName(instance.getProcessDefinitionName());
		
		api.setLocalizedName(null);
		api.setLocalizedDescription(null);
		api.setParentId(null);
		api.setProcessDefinitionId(instance.getProcessDefinitionId());
		api.setProcessDefinitionKey(instance.getProcessDefinitionKey());
		api.setProcessDefinitionName(instance.getProcessDefinitionName());
		api.setProcessDefinitionVersion(instance.getProcessDefinitionVersion());
		
		api.setProcessInstanceId(instance.getId());
		
		api.setRootProcessInstanceId(null);
		api.setStartTime(instance.getStartTime());
		api.setStartUserId(instance.getStartUserId());
		api.setSuperExecutionId(null);
		api.setTenantId(instance.getTenantId());
		
		
		api.setProcessVariables(instance.getProcessVariables());	
		
//		try {
//			api.setProcessVariables(ProcessEngines.getDefaultProcessEngine().getRuntimeService().getVariables(instance.getId()));	
//		}catch(ActivitiObjectNotFoundException ne) {
//			api.setProcessVariables(instance.getProcessVariables());	
//		}
		
		
		
		
		
		if(api.getProcessVariables()!=null) {
			for(String key:api.getProcessVariables().keySet()) {
				if(api.getProcessVariables().get(key)!=null && api.getProcessVariables().get(key) instanceof Date) {
					Date d = (Date)api.getProcessVariables().get(key);
					api.getProcessVariables().put(key+"formatted", sdf.format(d));
				}
			}
		}
		
		
		
		
		return api;
		
	}
}
